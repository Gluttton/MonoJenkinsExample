using System;
using NUnit.Framework;

namespace ExampleUTest
{
	[TestFixture()]
	public class ExampleCoreTest
	{
		ExampleCore.Example core;

		[TestFixtureSetUp]
		public void HelperTestSetUp ()
		{	
			core = new ExampleCore.Example ();
		}


		[Test()]
		public void ConcatenationSuccess ()
		{
			StringAssert.IsMatch ("Result", core.Concatenation ("Re", "sult") );
			StringAssert.AreEqualIgnoringCase ("Result", core.Concatenation ("Re", "sult") );
			StringAssert.Contains ("Result", core.Concatenation ("Re", "sult") );
			StringAssert.EndsWith ("Result", core.Concatenation ("Re", "sult") );
			StringAssert.StartsWith ("Result", core.Concatenation ("Re", "sult") );
		}
		
		
		[Test()]
		public void SumSuccess ()
		{
			Assert.AreEqual (0, Convert.ToUInt64 (core.Sum ("0", "0") ) );
			Assert.AreEqual (1, Convert.ToUInt64 (core.Sum ("0", "1") ) );
			Assert.AreEqual (42, Convert.ToUInt64 (core.Sum ("40", "2") ) );
		}
		

		[TestFixtureTearDown]
		public void ExampleCoreTestTearDown ()
		{
		}
	}
}

