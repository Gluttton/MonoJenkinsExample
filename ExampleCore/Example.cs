using System;

namespace ExampleCore
{
    /// <summary>
    /// Class which implement all business logic.
    /// </summary>
    /// <remarks>
    /// Actualy class implement just concatenation, but soon it is going
    /// to became more usefull.
    /// </remarks>
	public class Example
	{
        // TODO: Implement method wich processed summ.
		public Example ()
		{
		}
		
        // FIXME: Add comments!
		public string Concatenation (string first, string second)
		{
			return first + second;
		}
		
        // FIXME: Add comments!
		// FIXME: Add handling of possible exceptions!
		public string Sum (string first, string second)
		{
			return (Convert.ToInt64 (first) + Convert.ToInt64 (second) ).ToString ();
		}
	}
}

