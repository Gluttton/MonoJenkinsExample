<%@ Page Language="C#" Inherits="ExampleGUI.Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
    <!-- INFO: This code is present for CloneAnalyzer.-->
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
	<form id="formMain" runat="server">
		<table>
			<tr>
				<td> <asp:Label id="labelFirstOperand" text="First operand: " runat="server"/> </td>
				<td> <asp:TextBox id="textBoxFirstOperand" runat="server"/> </td>
			</tr>
			<tr>
				<td> <asp:Label id="labelSecondOperand" text="Second operand: " runat="server"/> </td>
				<td> <asp:TextBox id="textBoxSecondOperand" runat="server"/> </td>
			</tr>
			<tr>
				<td colspan="2"> <asp:Label id="labelResultCaption" text="Result: " runat="server"/> </td>
			</tr>
			<tr>
				<td colspan="2"> <asp:Label id="labelResult" text="" runat="server"/> </td>
			</tr>
			<tr>
				<td colspan="2"> <asp:Button id="buttonConcatenation" runat="server" Text="Concatenation" OnClick="buttonConcatenationClicked"/> </td>
			</tr>
			<tr>
				<td colspan="2"> <asp:Button id="buttonSum" runat="server" Text="Sum" OnClick="buttonSumClicked"/> </td>
			</tr>

		</table>
	</form>
    <!-- INFO: This code is present for CloneAnalyzer.-->
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>

</body>
</html>
