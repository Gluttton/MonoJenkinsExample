using System;
using System.Web;
using System.Web.UI;

namespace ExampleGUI
{
    /// <summary>
    /// Class of Default application page.
    /// </summary>
    /// <remarks>
    /// That's all...
    /// </remarks>
	public partial class Default : System.Web.UI.Page
	{
        // FIXME: Create correct comment instead of this.
        /// <summary>
        /// Event occurs when button with label "Concatenation"
        /// will be pushed.
        /// </summary>
        /// <param name="sender">
        /// Event sender.
        /// <see cref="System.Object"/>
        /// </param>
        /// <param name="e">
        /// Set of arguments.
        /// <see cref="System.Web.UI.WebControls.LoginCancelEventArgs"/>
        /// </param>
        /// <remarks>
        /// Call processing of concatenation.
        /// </remarks>
		public virtual void buttonConcatenationClicked (object sender, EventArgs args)
		{
			ExampleCore.Example core = new ExampleCore.Example ();
			
			labelResultCaption.Text = "Result of concatenation: ";
			labelResult.Text = core.Concatenation (textBoxFirstOperand.Text, textBoxSecondOperand.Text);
		}
		
		// FIXME: Add comment.
		public virtual void buttonSumClicked (object sender, EventArgs args)
		{
			ExampleCore.Example core = new ExampleCore.Example ();
			
			labelResultCaption.Text = "Result of sum: ";
			labelResult.Text = core.Sum (textBoxFirstOperand.Text, textBoxSecondOperand.Text);
		}
	}
}

